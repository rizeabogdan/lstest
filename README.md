what I did:
-remove unnecesary files (gradle etc)
-changed structure according to Action Classes pattern
-split feature file in 2, onefor positive and one for negative tests
-model class for products to deserialize
-fixed/changed tests 
-3 scenarios and one scenario outline for the available keywords 
note: outline scenarios fail on purpose (because not all the product name contain the keyword)
-setup gitlab pipeline to run tests and publish serenity report to https://rizeabogdan.gitlab.io/lstest/

How to:
-clone repository
-open in Intellij (and wait for ide to load maven project)
-to execute tests run command: mvn clean verify 

Write tests:
-write the test scenario in a feature file, using gherkin language (given, when, then)
-write method for each step and add cucumber annotation and step definition from feature file
