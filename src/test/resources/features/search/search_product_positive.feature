Feature: Search for product - positive scenarios

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario: Search for valid keyword
    Given  a search for apple is performed
    Then the response has status code 200

  Scenario Outline: Search for an available product
    Given a search for <keyword> is performed
    Then all titles of results are related to <keyword>
    Examples:
      | keyword |
      | mango   |
      | apple   |
      | tofu    |
      | water   |