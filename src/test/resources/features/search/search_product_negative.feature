Feature: Search for product - negative scenarios

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario: Calling endpoint with no param is refused
    Given a call on search endpoint with no parameter is done
    Then a not authorized response is returned

  Scenario: Search for an unavailable product
    Given a search for car is performed
    Then a 404 not found error is returned