package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.common.mapper.TypeRef;
import models.Product;
import models.Products;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.*;


public class SearchStepDefinitions {

    @Steps
    SearchAction search;

    @Given("a call on search endpoint with no parameter is done")
    public void call_search_endpoint_with_no_param() {
        search.withNoParameter().onEndpoint();
    }

    @Then("a not authorized response is returned")
    public void not_authorized_response_returned() {
        restAssuredThat(response -> response.statusCode(401).body("detail", is("Not authenticated")));
    }

    @Given("a search for {word} is performed")
    public void call_search_endpoint_with_param(String keyword) {
        search.forParameter(keyword).onEndpoint();
    }

    @Then("the response has status code {int}")
    public void the_response_has_status_code(int sc) {
        restAssuredThat(response -> response.statusCode(sc));
    }

    @Then("all titles of results are related to {word}")
    public void all_results_are_related_to(String keyword) {
        Products products = new Products(lastResponse()
                .as(new TypeRef<>() {
                }));
        for (Product product : products) {

            /*
            assertThat(product.getTitle().toLowerCase().contains(keyword));
            for some reason assertThat does not fail tests if the condition is false so had to use the below
             */
            Assert.assertTrue("Not all titles contain: " + keyword, product.getTitle().contains(keyword));
        }
    }

    @Then("a 404 not found error is returned")
    public void a_404_not_found_error_is_returned() {
        restAssuredThat(response -> response.body("detail.error", is(true)).statusCode(404));
    }
}
