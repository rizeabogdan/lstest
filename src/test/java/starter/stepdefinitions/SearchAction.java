package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class SearchAction {
    String keyword;

    public SearchAction forParameter(String keyword) {
        this.keyword = keyword;
        return this;
    }

    public SearchAction withNoParameter() {
        this.keyword = "";
        return this;
    }

    @Step("Perform search for #keyword")
    public SearchAction onEndpoint() {
        SerenityRest.given().get("https://waarkoop-server.herokuapp.com/api/v1/search/test/" + keyword);
        return this;

    }
}
